import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ICollection } from "../shared/interfaces";

interface ICollectionState {
  value: ICollection[];
  selectedCollection: ICollection | null;
}

const initialState: ICollectionState = {
  value: [],
  selectedCollection: null
};

export const collectionsSlice = createSlice({
  name: "collections",
  initialState,
  reducers: {
    // Create
    addCollection: (state, action: PayloadAction<ICollection>) => {
      state.value.push(action.payload);
    },
    // Delete
    removeCollection: (state, action: PayloadAction<number>) => {
      state.value.splice(action.payload, 1);
    },
    // Read all
    setCollections: (state, action: PayloadAction<ICollection[]>) => {
      state.value = action.payload;
    },
    // set selected collection
    selectCollection: (state, action: PayloadAction<string>) => {
      state.selectedCollection =
        state.value.find(collection => collection._id === action.payload) ||
        null;
    }
  }
});

// export the actions
export const {
  addCollection,
  removeCollection,
  setCollections,
  selectCollection
} = collectionsSlice.actions;

// export the reducer by default
export default collectionsSlice.reducer;
