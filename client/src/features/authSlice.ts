import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IUser } from "../shared/interfaces";

interface IAuthState {
  token: string | null;
  isAuth: boolean | null;
  user: IUser | null;
  alertMessage: string | null;
  loading: boolean;
}

const initialState: IAuthState = {
  token: localStorage.getItem("token"),
  isAuth: null,
  user: null,
  alertMessage: null,
  loading: true
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setLogin: (state, action: PayloadAction<any>) => {
      localStorage.setItem("token", action.payload);
      state.token = action.payload;
    },
    setError: (state, action: PayloadAction<any>) => {
      state.alertMessage = action.payload.msg;
    },
    resetError: (state, action: PayloadAction<any>) => {
      state.alertMessage = null;
    },
    setUser: (state, action: PayloadAction<IUser>) => {
      state.user = action.payload;
    },
    logOut: (state, action: PayloadAction<any>) => {
      state.token = null;
      localStorage.removeItem("token");
    }
  }
});

// export the actions
export const {
  setLogin,
  setError,
  setUser,
  logOut,
  resetError
} = authSlice.actions;

// export the reducer by default
export default authSlice.reducer;
