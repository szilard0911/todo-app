import { Schema, Document, model, Model } from "mongoose";

export interface ITodo extends Document {
  name: string;
  completed: boolean;
  creationDate: Date;
  collectionId: Schema.Types.ObjectId | string;
}

const TodoSchema: Schema = new Schema({
  name: {
    type: String,
    require: true,
    trim: true
  },
  completed: {
    type: Boolean,
    require: true,
    default: false
  },
  tags: {
    type: [String],
    require: false
  },
  creationDate: {
    type: Date,
    default: Date.now()
  },
  collectionId: {
    type: Schema.Types.ObjectId,
    ref: "Collection"
  }
});

export const TodoModel: Model<ITodo> = model<ITodo>("Todo", TodoSchema);
