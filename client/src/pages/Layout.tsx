import { useSelector, useDispatch } from "react-redux";
import { Outlet } from "react-router-dom";
import { RootState } from "../app/store";
import { resetError } from "../features/authSlice";

const Layout = () => {
  const alertMessage = useSelector(
    (state: RootState) => state.auth.alertMessage
  );
  const dispatch = useDispatch();
  return (
    <div className="min-h-screen font-helvetica">
      {alertMessage && (
        <div className="bg-new-red text-white uppercase text-base py-3 tracking-wider text-center flex justify-between">
          <p className="inline ml-10">{alertMessage}</p>
          <button
            className="mr-10"
            onClick={(event) => dispatch(resetError(true))}
          >
            Close
          </button>
        </div>
      )}
      <div>
        <Outlet />
      </div>
    </div>
  );
};
export default Layout;
