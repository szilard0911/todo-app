import { check, query } from "express-validator";
import { Router } from "express";

import AuthMiddleware from "../middleware/authMiddleware";
import CollectionMiddleware from "../middleware/collectionMiddleware";
import TodoController from "../controllers/todoController";

const router = Router();

router.post(
  "/",
  AuthMiddleware.validateAuthToken,
  [
    check("name", "The name is required").not().isEmpty(),
    check("collectionId", "The collectionId is required").not().isEmpty()
  ],
  CollectionMiddleware.validateCollection, // this middleware has to be after the check because the id is required to validate the collection
  TodoController.createTodo
);
router.get(
  "/",
  AuthMiddleware.validateAuthToken,
  [query("collectionId", "The collectionId is required").not().isEmpty()],
  CollectionMiddleware.validateCollection,
  TodoController.getTodos
);
router.put(
  "/:id",
  AuthMiddleware.validateAuthToken,
  [check("collectionId", "The collectionId is required").not().isEmpty()],
  CollectionMiddleware.validateCollection,
  TodoController.updateTodo
);
router.delete(
  "/:id",
  AuthMiddleware.validateAuthToken,
  [check("collectionId", "The collectionId is required").not().isEmpty()],
  CollectionMiddleware.validateCollection,
  TodoController.deleteTodo
);

export default router;
