export class Constants {
  public static readonly APIBaseURL: string = "";
  public static readonly APIPort: string = "";
  public static readonly APIBaseURL_dev: string = "http://localhost:4000/api/";
  public static readonly APIPort_dev: string = "4000";
}
