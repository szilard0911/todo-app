export interface ButtonItem {
  key: string;
  label: string;
}

export interface ILoginUser {
  email: string;
  password: string;
}

export interface IRegisterUser extends ILoginUser {
  name: string;
  confirmPassword: string;
}

export interface ICollection {
  _id: string;
  name: string;
  owner: string;
}

export interface ITodo {
  _id: string;
  name: string;
  completed: boolean;
  tags: string[];
  collectionId: string;
}

export interface IUser {
  _id: string;
  name: string;
  email: string;
}
