import axios, { AxiosError } from "axios";
import React, { ChangeEvent, MouseEvent } from "react";
import { useDispatch } from "react-redux";
import { removeTodo, setTodo } from "../features/todoSlice";
import { Constants } from "../shared/constants";
import { ITodo } from "../shared/interfaces";
import { useState } from "react";
import { logOut, setError } from "../features/authSlice";
import { useNavigate } from "react-router-dom";

interface TodoCardTypes extends ITodo {
  index: number;
}

const TodoCard = (todo: TodoCardTypes) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  //set temporary todo for edit
  const [tempTodo, setTempTodo] = useState({
    _id: todo._id,
    completed: todo.completed,
    name: todo.name,
    tags: todo.tags,
    collectionId: todo.collectionId
  } as ITodo);

  const [editMode, setEditMode] = useState(false);
  const [tagsInput, setTagsInput] = useState(todo.tags.join(", "));

  // Remove Todo
  const handleRemoveClick = (
    event: MouseEvent<HTMLButtonElement>,
    index: number,
    _id: string
  ) => {
    event.preventDefault();
    const data = {
      collectionId: todo.collectionId
    };
    axios
      .delete(Constants.APIBaseURL_dev + "todos/" + _id, {
        data: data,
        headers: {
          "x-auth-token": localStorage.getItem("token") || ""
        }
      })
      .then((response) => {
        console.log("response", response);
        dispatch(removeTodo(index));
      })
      .catch((error: AxiosError) => {
        console.log("error :>> ", error);
        if (error.response) {
          dispatch(setError(error.response.data));
        }
        if (error.response?.status === 401) {
          dispatch(logOut(true));
          navigate("/");
        }
      });
  };

  const toggleComplete = () => {
    setTempTodo({ ...tempTodo, completed: !tempTodo.completed });
    //editTask(task);
  };

  const handleEditClick = (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    setEditMode(true);
  };

  const handleCancelClick = (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    setTempTodo(todo);
    setTagsInput(todo.tags.join(", "));
    setEditMode(false);
  };

  const handleTagsOnChange = (event: ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    setTagsInput(event.target.value);
    let tags: string[] = event.target.value.split(",");
    tags = tags.map((tag) => tag.trim());
    tags = tags.filter((tag) => tag !== "");
    setTempTodo({ ...tempTodo, tags: tags });
  };

  const handleSaveClick = (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();

    //here goes the saving method

    axios
      .put(Constants.APIBaseURL_dev + "todos/" + tempTodo._id, tempTodo, {
        headers: {
          "x-auth-token": localStorage.getItem("token") || ""
        }
      })
      .then((response) => {
        dispatch(setTodo(tempTodo));
        setTagsInput(tempTodo.tags.join(", "));
        setEditMode(false);
      })
      .catch((error: AxiosError) => {
        console.log("error :>> ", error);
        if (error.response) {
          dispatch(setError(error.response.data));
        }
        if (error.response?.status === 401) {
          dispatch(logOut(true));
          navigate("/");
        }
      });
  };

  return (
    <div className="collectionCard p-6 my-4 max-w-sm bg-white rounded-lg border border-gray-200 shadow-md hover:bg-gray-100 ">
      {editMode ? (
        <>
          <input
            type="text"
            className="w-full mb-2 block py-2 px-3 text-base text-new-form-grey border border-new-green-light border-opacity-50 bg-new-green-light bg-opacity-50 shadow-sm focus:bg-white focus:outline-none focus:ring-0 focus:border-new-green tracking-wider sm:text-base lg:text-xs"
            value={tempTodo.name}
            onChange={(event) => {
              event.preventDefault();
              setTempTodo({ ...tempTodo, name: event.target.value });
            }}
          />
          <div className="status">
            {tempTodo.completed ? (
              <button
                type="button"
                className="complete rounded-lg py-1 px-2 mb-2 text-white text-base bg-new-green tracking-wider sm:text-base lg:text-xs"
                onClick={toggleComplete}
              >
                Complete
              </button>
            ) : (
              <button
                type="button"
                className="incomplete rounded-lg py-1 px-2 mb-2 text-white text-base bg-new-red tracking-wider sm:text-base lg:text-xs"
                onClick={toggleComplete}
              >
                Incomplete
              </button>
            )}
          </div>
          <div className="tags">
            <input
              placeholder="Separete tags with comma(,) character."
              className="w-full mb-2 block py-2 px-3 text-base text-new-form-grey border border-new-green-light border-opacity-50 bg-new-green-light bg-opacity-50 shadow-sm focus:bg-white focus:outline-none focus:ring-0 focus:border-new-green tracking-wider sm:text-base lg:text-xs"
              type="text"
              value={tagsInput}
              onChange={(event) => handleTagsOnChange(event)}
            />
          </div>
          <div>
            <button
              className="rounded-lg py-2 px-4 mr-4 text-white text-base focus:outline-none uppercase bg-new-green tracking-wider sm:text-base lg:text-xs"
              type="button"
              onClick={(event) => handleSaveClick(event)}
            >
              Save
            </button>
            <button
              type="button"
              className="rounded-lg py-2 px-4 text-white text-base focus:outline-none uppercase bg-new-red tracking-wider sm:text-base lg:text-xs"
              onClick={(event) => handleCancelClick(event)}
            >
              Cancel
            </button>
          </div>
        </>
      ) : (
        <div>
          <p className="mb-4">{tempTodo.name}</p>
          <div className="status mb-5">
            {tempTodo.completed ? (
              <span className="complete rounded-lg py-1 px-2 text-white text-base bg-new-green tracking-wider sm:text-base lg:text-xs">
                Complete
              </span>
            ) : (
              <span className="incomplete rounded-lg py-1 px-2 text-white text-base bg-new-red tracking-wider sm:text-base lg:text-xs">
                Incomplete
              </span>
            )}
          </div>
          <div className="tags">
            {tempTodo.tags.length ? (
              <div className="mb-5">
                {tempTodo.tags.map((tag, index) => {
                  return (
                    <span
                      key={tag + "-" + index}
                      className="inline text-xs py-1 px-2 mx-1 rounded-xl border border-grey-100 hover:bg-gray-200"
                    >
                      {tag}
                    </span>
                  );
                })}
              </div>
            ) : (
              <p className="mb-5 text-xs py-1 px-2">There are no tags</p>
            )}
          </div>
          <button
            className="rounded-lg py-2 px-4 mr-4 text-white text-base focus:outline-none uppercase bg-new-green tracking-wider sm:text-base lg:text-xs"
            type="button"
            onClick={(event) => handleEditClick(event)}
          >
            Edit
          </button>
          <button
            className="rounded-lg py-2 px-4 text-white text-base focus:outline-none uppercase bg-new-red tracking-wider sm:text-base lg:text-xs"
            type="button"
            onClick={(event) => handleRemoveClick(event, todo.index, todo._id)}
          >
            Remove
          </button>
        </div>
      )}
    </div>
  );
};
export default TodoCard;
