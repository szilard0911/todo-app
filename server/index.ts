import cors from "cors";
import express, { Express, json } from "express";

import { connectDB } from "./config/db";
import usersRouter from "./routes/users";
import authRouter from "./routes/auth";
import collectionsRouter from "./routes/collections";
import todosRouter from "./routes/todo";

// create the server
const app: Express = express();
const PORT = process.env.PORT || 4000;

// connect to the database
connectDB();

// enable cors
app.use(cors());

// parsing incoming requests to JSON
app.use(json());

// Registration
app.use("/api/users", usersRouter);
// Login
app.use("/api/auth", authRouter);
// Collections
app.use("/api/collections", collectionsRouter);
// Todos
app.use("/api/todos", todosRouter);

app.listen(PORT, () => {
  console.log("server runing on", PORT);
});
