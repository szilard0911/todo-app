import { ITodo, TodoModel } from "../models/Todos";
// import { ICollection, CollectionModel } from "../models/Collection";

class TodoController {
  // Create a todo
  async createTodo(req: any, res: any) {
    try {
      const todo: ITodo = new TodoModel(req.body);
      await todo.save();
      res.json(todo);
    } catch (error) {
      console.log(error);
      res.status(500).send("Unexpected error while creating todo");
    }
  }
  // Get all todos by collection id
  async getTodos(req: any, res: any) {
    try {
      const { collectionId } = req.query;
      const todos = await TodoModel.find({ collectionId: collectionId }).sort({
        $natural: -1
      });
      res.json(todos);
    } catch (error) {
      console.log(error);
      res.status(500).send("Unexpected error while geting todos");
    }
  }

  // Update todo
  async updateTodo(req: any, res: any) {
    try {
      const { name, completed, tags } = req.body;

      // Check if the todo exists
      let todo = await TodoModel.findById(req.params.id);
      if (!todo) res.status(400).json({ msg: "Todo does not exists" });

      const newTodo: any = {};
      newTodo.name = name;
      newTodo.completed = completed;
      newTodo.tags = tags;

      // update todo
      todo = await TodoModel.findOneAndUpdate({ _id: req.params.id }, newTodo, {
        new: true
      });
      res.json({ todo });
    } catch (error) {
      console.log(error);
      res.status(500).send("Unexpected error while updating todo");
    }
  }
  // Delete todo
  async deleteTodo(req: any, res: any) {
    try {
      // Check if the todo exists
      let todo = await TodoModel.findById(req.params.id);
      if (!todo) res.status(400).json({ msg: "Todo does not exists" });

      await TodoModel.findByIdAndRemove(req.params.id);
      res.json({ msg: "Todo deleted" });
    } catch (error) {
      console.log(error);
      res.status(500).send("Unexpected error while deleting todos");
    }
  }
}

export default new TodoController();
