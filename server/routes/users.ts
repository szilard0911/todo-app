import { check } from "express-validator";
import { Router } from "express";

import UserController from "../controllers/userController";

const router = Router();

// Registration
router.post(
  "/",
  [
    check("name", "The Name is required").not().isEmpty(),
    check("email", "Add a valid Email").isEmail(),
    check("password")
      .isLength({ min: 6 })
      .withMessage("Password must be 6 characters long at least.")
      .matches(/\d/)
      .withMessage("Password must contain a number")
  ],
  UserController.createUser
);

export default router;
