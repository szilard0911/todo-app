import { configureStore } from "@reduxjs/toolkit";
import collectionsReducer from "../features/collectionSlice";
import authReducer from "../features/authSlice";
import todosReducer from "../features/todoSlice";

export const store = configureStore({
  reducer: {
    collections: collectionsReducer,
    auth: authReducer,
    todos: todosReducer
  }
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
