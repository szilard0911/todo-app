# Todo App

### Prerequisits
-   Install  node v17.9.0 or a higher version.
### Installation

-   Git clone this repository
-   Run `yarn` or `npm install` inside the client folder to install all dependencies.
-   Run `yarn` or `npm install` inside the server folder to install all dependencies.
-   Create your own MongoDB Atlas Database [here](https://www.mongodb.com/cloud/atlas).
-   Add the Database connection string to server/variables.env file.
-   Add your (random) secret key for JSON web tokens to your server/variables.env file.

```
# server/variables.env file example:
DB_MONGO=mongodb+srv://<user>:<passeord>@cluster0.enjxv.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
SECRET_KEY=your_secret_key
```

-   Add the backend url to your .env file.

```
# client/.env.development.local file
REACT_APP_BACKEND_URL=http://localhost:4000
```

-   Start the react development server: `cd client && yarn start`.
-   Start the nodejs server: `cd server && yarn run dev`.
-   The client application will run on http://localhost:3000 
-   You can import the postman collection to test the server only.
