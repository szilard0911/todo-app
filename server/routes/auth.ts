import AuthMiddleware from "../middleware/authMiddleware";
import { check } from "express-validator";
import { Router } from "express";

import AuthController from "../controllers/authcontroller";

const router = Router();

// Login
router.post(
  "/",
  [
    check("email", "Add a valid Email").isEmail(),
    check("password", "The Password must be at least 6 characters").isLength({
      min: 6
    })
  ],
  AuthController.authUser
);

// Get User
router.get("/", AuthMiddleware.validateAuthToken, AuthController.getAuthUser);

export default router;
