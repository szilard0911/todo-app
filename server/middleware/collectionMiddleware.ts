import express from "express";
import { Result, ValidationError, validationResult } from "express-validator";

import { CollectionModel } from "../models/Collection";

//This middleware checks the existance and ownership of the given collection
class CollectionMiddleware {
  async validateCollection(
    req: any,
    res: express.Response,
    next: express.NextFunction
  ) {
    // Apply the validation which is missing from the controller
    const errors: Result<ValidationError> = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      // check the collection is exits, /the id can be in the in the body and in the query parameters/
      const collectionId = req.body.collectionId || req.query.collectionId;
      const collection = await CollectionModel.findById(collectionId);
      if (!collection) {
        return res.status(404).json({ msg: "Collection does not exists." });
      }
      // check the user is the owner of the collection
      if (collection.owner.toString() !== req.user.id) {
        return res.status(401).json({ msg: "Not authorized" });
      }
      next();
    } catch (error) {
      console.log(error);
      res.status(500).send("Unexpected error while todo operation.");
    }
  }
}

export default new CollectionMiddleware();
