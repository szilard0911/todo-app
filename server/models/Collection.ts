import { Schema, Document, model, Model } from "mongoose";

export interface ICollection extends Document {
  name: string;
  owner: Schema.Types.ObjectId | string;
}

const CollectionSchema: Schema = new Schema({
  name: {
    type: String,
    require: true,
    trim: true
  },
  owner: {
    type: Schema.Types.ObjectId,
    ref: "User"
  },
  creationDate: {
    type: Date,
    default: Date.now()
  }
});

export const CollectionModel: Model<ICollection> = model<ICollection>(
  "Collection",
  CollectionSchema
);
