import { compare } from "bcryptjs";
import { Result, ValidationError, validationResult } from "express-validator";
import jwt from "jsonwebtoken";
import { UserModel } from "../models/User";

class AuthController {
  // Login
  async authUser(req: any, res: any) {
    const errors: Result<ValidationError> = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const { email, password } = req.body;
      // query the user
      let user = await UserModel.findOne({ email });
      if (!user) {
        return res.status(400).json({ msg: "User does not exist" });
      }

      // check the password
      const correctPassword = await compare(password, user.password);
      if (!correctPassword) {
        return res.status(400).json({ msg: "Incorrect password" });
      }

      // create and set the auth token
      const payload = {
        user: {
          id: user.id
        }
      };
      jwt.sign(
        payload,
        `${process.env.SECRET_KEY}`,
        {
          expiresIn: 3600
        },
        (error, token) => {
          if (error) throw error;

          res.json({ token });
        }
      );
    } catch (error) {
      console.log(error);
    }
  }

  // Get user
  async getAuthUser(req: any, res: any) {
    try {
      // get the user data exclude the password
      const user = await UserModel.findById(req.user.id).select("-password");
      res.json({ user });
    } catch (error) {
      console.log(error);
      return res
        .status(500)
        .json({ msg: "An unexpected error occurred while querying the user" });
    }
  }
}

export default new AuthController();
