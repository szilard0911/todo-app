import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';


const useAxios = (axiosParams: AxiosRequestConfig, toDispatch?: (arg0: any) => {type: any, response: any}) => {
  const dispatch = useDispatch();
  const [response, setResponse] = useState<AxiosResponse>();
  const [error, setError] = useState<AxiosError>();
  const [loading, setLoading] = useState<boolean>(true);
  

  const fetchData = async (params: AxiosRequestConfig, controller:AbortController) => {
    params.signal = controller.signal;
    try {
      if((params.method === 'POST' && params.data) || params.method === 'GET' ){
        await axios.request(params).then((response:AxiosResponse) => {
          if(toDispatch){
            dispatch(toDispatch(response));
          }
          setResponse(response);
          setError(undefined);
        });
      }

      } catch( err:any ) {

        // Commented out because it made for development purpose only.
        // if (err.response) {
        //   // The request was made and the server responded with a status code
        //   // that falls out of the range of 2xx
        //   console.log(err.response.data);
        //   console.log(err.response.status);
        //   console.log(err.response.headers);
        // } else if (err.request) {
        //   // The request was made but no response was received
        //   // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        //   // http.ClientRequest in node.js
        //   console.log(err.request);
        // } else {
        //   // Something happened in setting up the request that triggered an Error
        //   console.log('Error', err.message);
        // }

        setError(err);
      } finally {
        setLoading(false);
      }
      
 };

  useEffect(() => {
    const controller:AbortController = new AbortController();
    fetchData(axiosParams, controller);
    // useEffect cleanup function
    return () => controller.abort();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[axiosParams.url, axiosParams.data]);

  return {response, error, loading}
}

export default useAxios