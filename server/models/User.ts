import { Schema, Document, model, Model } from "mongoose";

export interface IUser extends Document {
  name: string;
  email: string;
  password: string;
  registrationDate: Date;
}

const UsersSchema: Schema = new Schema({
  name: {
    type: String,
    require: true,
    trim: true
  },
  email: {
    type: String,
    require: true,
    trim: true,
    unique: true
  },
  password: {
    type: String,
    require: true,
    trim: true
  },
  registrationDate: {
    type: Date,
    default: Date.now()
  }
});

export const UserModel: Model<IUser> = model<IUser>("User", UsersSchema);
