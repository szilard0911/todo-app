import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import "./App.scss";
import Collections from "./pages/Collections";
import Layout from "./pages/Layout";
import LoginPage from "./pages/Login";
import NoMatch from "./pages/NoMatch";
import RegisterPage from "./pages/Register";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/collections" element={<Collections />} />
          <Route path="*" element={<NoMatch />} />
        </Route>
      </Routes>
    </Router>
  );
}

export default App;
