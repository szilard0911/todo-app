import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ITodo } from "../shared/interfaces";

interface ITodoState {
  currentTodos: ITodo[];
}

const initialState: ITodoState = {
  currentTodos: []
};

export const todosSlice = createSlice({
  name: "todos",
  initialState,
  reducers: {
    // Create
    addTodo: (state, action: PayloadAction<ITodo>) => {
      state.currentTodos.push(action.payload);
    },
    // Delete
    removeTodo: (state, action: PayloadAction<number>) => {
      state.currentTodos.splice(action.payload, 1);
    },
    // Read all
    setTodos: (state, action: PayloadAction<ITodo[]>) => {
      state.currentTodos = action.payload;
    },
    // Update
    setTodo: (state, action: PayloadAction<ITodo>) => {
      state.currentTodos = state.currentTodos.map(
        todo => (todo._id === action.payload._id ? action.payload : todo)
      );
    }
  }
});

// export the actions
export const { addTodo, removeTodo, setTodos, setTodo } = todosSlice.actions;

// export the reducer by default
export default todosSlice.reducer;
