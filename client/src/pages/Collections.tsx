//import Products from "../components/products";

import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../app/store";
import CollectionCard from "../components/CollectionCard";
import { useState, useEffect } from "react";
import { addCollection, setCollections } from "../features/collectionSlice";
import { Constants } from "../shared/constants";
import useAxios from "../hooks/useAxios";
import { useNavigate } from "react-router-dom";
import axios, { AxiosError } from "axios";
import { ICollection } from "../shared/interfaces";
import { logOut, setError } from "../features/authSlice";
import Todos from "../components/Todos";

const Collections = () => {
  const collections = useSelector(
    (state: RootState) => state.collections.value
  );
  const [collectionNameInput, setCollectionNameInput] = useState("");

  const dispatch = useDispatch();
  const navigate = useNavigate();

  // Create Collection
  const handleCreateCollection = () => {
    if (!collectionNameInput) return;
    // dispatch the action that adds the new collection to the redux store. (this has to happen after it has been added to the db without error)
    const data = {
      name: collectionNameInput
    };
    axios
      .post(Constants.APIBaseURL_dev + "collections/", data, {
        headers: {
          "x-auth-token": localStorage.getItem("token") || ""
        }
      })
      .then((response) => {
        const newCollection: ICollection = {
          _id: response.data._id,
          name: response.data.name,
          owner: response.data.owner
        };
        dispatch(addCollection(newCollection));
      })
      .catch((error: AxiosError) => {
        console.log("error :>> ", error);
        if (error.response) {
          dispatch(setError(error.response.data));
        }
        if (error.response?.status === 401) {
          dispatch(logOut(true));
          navigate("/");
        }
      })
      .finally(() => {
        // reset the input field (maybe dispatch a success message, the rror message sould be in the layout)
        setCollectionNameInput("");
      });
  };

  // Load the collections
  const { response, loading, error } = useAxios({
    method: "GET",
    baseURL: Constants.APIBaseURL_dev,
    url: "collections",
    headers: {
      accept: "application/json",
      "x-auth-token": localStorage.getItem("token") || ""
    }
  });
  // loguot and redirect when the status unauthorized
  if (error) {
    if (error.response?.status === 401) {
      dispatch(logOut(true));
      navigate("/");
    }
  }
  // dispatch on mount and when the response changes (it wont change but if we would have a some filter it could)
  useEffect(() => {
    if (!error && !loading && response) {
      dispatch(setCollections(response.data));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [response]);

  return (
    <div className="collectionsContainer">
      <div className="flex justify-center bg-new-green-light bg-opacity-50 px-5 sm:px-5 lg:px-10">
        <p className="text-new-green uppercase text-base py-3 tracking-wider">
          Collections & ToDos
        </p>
      </div>
      <div className="mt-10 sm:mt-0 flex justify-center">
        <div className="w-full sm:w-1/2 lg:w-1/3 mx-4">
          <p className="text-new-green uppercase text-base py-3 tracking-wider">
            Collections
          </p>
          {collections.map((collection, index) => (
            <CollectionCard
              key={collection._id}
              name={collection.name}
              index={index}
              _id={collection._id}
            />
          ))}

          <input
            className="mb-2 block w-full max-w-sm py-2 px-3 text-base text-new-form-grey border border-new-green-light border-opacity-50 bg-new-green-light bg-opacity-50 shadow-sm focus:bg-white focus:outline-none focus:ring-0 focus:border-new-green tracking-wider sm:text-base lg:text-xs"
            value={collectionNameInput}
            onChange={(event) => {
              event.preventDefault();
              setCollectionNameInput(event.target.value);
            }}
          />
          <button
            type="button"
            className="rounded-full py-2 px-12 text-white text-base focus:outline-none uppercase bg-new-green tracking-wider sm:text-base lg:text-xs"
            onClick={handleCreateCollection}
          >
            Create Collection
          </button>
        </div>
        <div className="w-full sm:w-1/2 lg:w-1/3 mx-4">
          <Todos />
        </div>
      </div>
    </div>
  );
};

export default Collections;
