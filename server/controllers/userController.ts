import { genSalt, hash } from "bcryptjs";
import { Result, ValidationError, validationResult } from "express-validator";
import jwt from "jsonwebtoken";

import { UserModel } from "../models/User";

class UserController {
  async createUser(req: any, res: any) {
    const errors: Result<ValidationError> = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { email, password } = req.body;
    try {
      // check if the user already exists
      let user = await UserModel.findOne({ email });
      if (user) {
        return res.status(400).json({ msg: "User already exists" });
      }
      // re-initialize user if it not exists
      user = new UserModel(req.body);

      // generate password hash
      const salt = await genSalt(10);
      user.password = await hash(password, salt);

      // save the user
      await user.save();

      // create and set the auth token
      const payload = {
        user: {
          id: user.id
        }
      };
      jwt.sign(
        payload,
        `${process.env.SECRET_KEY}`,
        {
          expiresIn: 3600
        },
        (error, token) => {
          if (error) throw error;
          res.json({ token });
        }
      );
    } catch (error) {
      console.log(error);
      res
        .status(400)
        .send("An unexpected error occurred during user registration.");
    }
  }
}

export default new UserController();
