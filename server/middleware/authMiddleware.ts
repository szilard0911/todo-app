import jwt from "jsonwebtoken";
import express from "express";

class AuthMiddleware {
  async validateAuthToken(
    req: any,
    res: express.Response,
    next: express.NextFunction
  ) {
    const token = req.header("x-auth-token");
    if (!token) {
      return res
        .status(401)
        .json({ msg: "There isn't auth token, invalid permission" });
    }
    try {
      const encryption: any = jwt.verify(token, `${process.env.SECRET_KEY}`);
      req.user = encryption.user;
      next();
    } catch (error) {
      res.status(401).json({ msg: "Invalid token" });
    }
  }
}

export default new AuthMiddleware();
