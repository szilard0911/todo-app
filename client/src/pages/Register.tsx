import { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import { Constants } from "../shared/constants";

import { logOut, setError, setLogin, setUser } from "../features/authSlice";
import { IRegisterUser } from "../shared/interfaces";
import axios, { AxiosError } from "axios";
import { Link } from "react-router-dom";

const RegisterPage = () => {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const inputClasses: string =
    "mb-2 block w-full py-2 px-3 text-base text-new-form-grey border border-new-green-light border-opacity-50 bg-new-green-light bg-opacity-50 shadow-sm focus:bg-white focus:outline-none focus:ring-0 focus:border-new-green tracking-wider sm:text-base lg:text-xs";

  // Custom hook to manage the entire form
  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues
  } = useForm<IRegisterUser>();

  // Redirect if the User is logged in
  useEffect(() => {
    if (localStorage.token) {
      navigate("/collections");
    }
  });

  const useOnSubmit: SubmitHandler<IRegisterUser> = (data) => {
    setLoading(true);
    axios
      .post(Constants.APIBaseURL_dev + "users", {
        name: data.name,
        email: data.email,
        password: data.password
      })
      .then((response) => {
        if (response.data.token) {
          dispatch(setLogin(response.data.token));
          // Get the user using the given token
          axios
            .get(Constants.APIBaseURL_dev + "auth", {
              headers: {
                "x-auth-token": response.data.token || ""
              }
            })
            .then((response) => {
              if (response.data.user) {
                dispatch(setUser(response.data.user));
                navigate("/collections");
              } else {
                dispatch(setError("User authentication failed."));
                dispatch(logOut);
              }
            });
          //////////////////////////////////////
        } else {
          dispatch(setError("User authentication failed."));
          dispatch(logOut);
        }
      })
      .catch((error: AxiosError) => {
        console.log("error :>> ", error);
        if (error.response) {
          dispatch(setError(error.response.data));
        }
        if (error.response?.status === 401) {
          dispatch(logOut(true));
          navigate("/");
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };

  // Set error messages
  if (errors.email) {
    switch (errors.email.type) {
      case "required":
        errors.email.message = "Email is required!";
        break;
      case "pattern":
        errors.email.message = "Invalid email address";
        break;
      default:
        break;
    }
  }
  if (errors.password) {
    switch (errors.password.type) {
      case "required":
        errors.password.message = "Password is required!";
        break;
      case "min-length":
        errors.password.message = "Password has to be 6 characters at least!";
        break;
      default:
        break;
    }
  }
  if (errors.confirmPassword) {
    switch (errors.confirmPassword.type) {
      case "required":
        errors.confirmPassword.message = "Password Confirmation is required!";
        break;
      case "validate":
        errors.confirmPassword.message =
          "Confirmation has to be to match with the password!";
        break;
      default:
        break;
    }
  }
  if (errors.name) {
    switch (errors.name.type) {
      case "required":
        errors.name.message = "Name is required!";
        break;
      case "min-length":
        errors.name.message = "Name has to be 4 characters at least!";
        break;
      default:
        break;
    }
  }

  return (
    <div className="loginContainer">
      <div className="flex justify-center bg-new-green-light bg-opacity-50 px-5 sm:px-5 lg:px-10">
        <p className="text-new-green uppercase text-base py-3 tracking-wider">
          Register
        </p>
      </div>
      <div className="mt-10 sm:mt-0 flex justify-center">
        <form
          onSubmit={handleSubmit(useOnSubmit)}
          className="w-full sm:w-1/2 lg:w-1/3"
        >
          <div>
            <div className="px-4 py-5 bg-white sm:p-6">
              <div className="relative flex flex-col">
                <label
                  htmlFor="name"
                  className="block text-new-form-grey-light tracking-wider"
                >
                  Name
                </label>
                <input
                  disabled={loading}
                  type="text"
                  id="name"
                  autoComplete="name"
                  className={`${inputClasses} ${errors.name ? "error" : ""}`}
                  placeholder="Name"
                  {...register("name", {
                    required: true,
                    minLength: 4
                  })}
                />
                {errors.name && (
                  <div className="lg:absolute lg:left-full lg:w-full lg:top-2.5 lg:flex lg:flex-col">
                    <span className=" error relative z-10 lg:pt-3 text-new-red text-xs leading-none whitespace-no-wrap tracking-wider">
                      {errors.name.message}
                    </span>
                  </div>
                )}
              </div>
              <div className="relative flex flex-col">
                <label
                  htmlFor="email"
                  className="block text-new-form-grey-light tracking-wider"
                >
                  Email
                </label>
                <input
                  disabled={loading}
                  type="text"
                  id="email"
                  autoComplete="email"
                  className={`${inputClasses} ${errors.email ? "error" : ""}`}
                  placeholder="Email"
                  {...register("email", {
                    required: true,
                    pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i
                  })}
                />
                {errors.email && (
                  <div className="lg:absolute lg:left-full lg:w-full lg:top-2.5 lg:flex lg:flex-col">
                    <span className=" error relative z-10 lg:pt-3 text-new-red text-xs leading-none whitespace-no-wrap tracking-wider">
                      {errors.email.message}
                    </span>
                  </div>
                )}
              </div>
              <div className="relative flex flex-col">
                <label
                  htmlFor="password"
                  className="block text-new-form-grey-light tracking-wider"
                >
                  Password
                </label>
                <input
                  disabled={loading}
                  type="password"
                  id="password"
                  autoComplete="password"
                  className={`${inputClasses} ${
                    errors.password ? "error" : ""
                  }`}
                  placeholder="Password"
                  {...register("password", {
                    required: true,
                    minLength: 6
                  })}
                />
                {errors.password && (
                  <div className="lg:absolute lg:left-full lg:w-full lg:top-2.5 lg:flex lg:flex-col">
                    <span className=" error relative z-10 lg:pt-3 text-new-red text-xs leading-none whitespace-no-wrap tracking-wider">
                      {errors.password.message}
                    </span>
                  </div>
                )}
              </div>
              <div className="relative flex flex-col">
                <label
                  htmlFor="confirmPassword"
                  className="block text-new-form-grey-light tracking-wider"
                >
                  Cofirm Password
                </label>
                <input
                  disabled={loading}
                  type="password"
                  id="confirmPassword"
                  autoComplete="confirmPassword"
                  className={`${inputClasses} ${
                    errors.confirmPassword ? "error" : ""
                  }`}
                  placeholder="Confirm Password"
                  {...register("confirmPassword", {
                    required: true,
                    validate: (value) => value === getValues().password
                  })}
                />
                {errors.confirmPassword && (
                  <div className="lg:absolute lg:left-full lg:w-full lg:top-2.5 lg:flex lg:flex-col">
                    <span className=" error relative z-10 lg:pt-3 text-new-red text-xs leading-none whitespace-no-wrap tracking-wider">
                      {errors.confirmPassword.message}
                    </span>
                  </div>
                )}
              </div>
            </div>
            <div className="px-4 py-3 grid justify-items-center">
              <button
                disabled={loading}
                type="submit"
                className="rounded-full py-2 px-12 text-white text-base focus:outline-none uppercase bg-new-green tracking-wider sm:text-base lg:text-xs"
              >
                Register
              </button>
              <Link
                className="text-base py-2 text-new-green sm:text-base lg:text-xs"
                to="/"
              >
                Login
              </Link>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default RegisterPage;
