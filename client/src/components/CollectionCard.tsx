import axios, { AxiosError } from "axios";
import React, { MouseEvent } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { logOut, setError } from "../features/authSlice";
import {
  removeCollection,
  selectCollection
} from "../features/collectionSlice";
import { setTodos } from "../features/todoSlice";
import { Constants } from "../shared/constants";

interface CollectionCardTypes {
  name: string;
  index: number;
  _id: string;
}

const CollectionCard = ({ name, index, _id }: CollectionCardTypes) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  // Delete Collection
  const handleRemoveClick = (
    event: MouseEvent<HTMLButtonElement>,
    index: number,
    _id: string
  ) => {
    event.preventDefault();
    axios
      .delete(Constants.APIBaseURL_dev + "collections/" + _id, {
        headers: {
          "x-auth-token": localStorage.getItem("token") || ""
        }
      })
      .then((response) => {
        console.log("response", response);
        dispatch(removeCollection(index));
      })
      .catch((error: AxiosError) => {
        console.log("error :>> ", error);
        if (error.response) {
          dispatch(setError(error.response.data));
        }
        if (error.response?.status === 401) {
          dispatch(logOut(true));
          navigate("/");
        }
      });
  };
  // Select Collection
  const handleCollectionClick = (
    event: MouseEvent<HTMLDivElement>,
    index: number,
    _id: string
  ) => {
    event.preventDefault();
    dispatch(selectCollection(_id));
    axios
      .get(Constants.APIBaseURL_dev + "todos?collectionId=" + _id, {
        headers: {
          "x-auth-token": localStorage.getItem("token") || ""
        }
      })
      .then((response) => {
        if (response.data) {
          dispatch(setTodos(response.data));
        } else {
          dispatch(setTodos([]));
        }
      })
      .catch((error: AxiosError) => {
        console.log("error :>> ", error);
        if (error.response) {
          dispatch(setError(error.response.data));
        }
        if (error.response?.status === 401) {
          dispatch(logOut(true));
          navigate("/");
        }
      });
  };

  return (
    <div
      onClick={(event) => handleCollectionClick(event, index, _id)}
      className="collectionCard cursor-pointer p-6 my-4 max-w-sm bg-white rounded-lg border border-gray-200 shadow-md hover:bg-gray-100 "
    >
      <p className="mb-4">{name}</p>
      <button
        className="rounded-lg py-2 px-4 text-white text-base focus:outline-none uppercase bg-new-green tracking-wider sm:text-base lg:text-xs"
        type="button"
        onClick={(event) => handleRemoveClick(event, index, _id)}
      >
        Remove
      </button>
    </div>
  );
};
export default CollectionCard;
