import { check } from "express-validator";
import { Router } from "express";

import AuthMiddleware from "../middleware/authMiddleware";
import CollectionController from "../controllers/collectionController";

const router = Router();

router.post(
  "/",
  AuthMiddleware.validateAuthToken,
  [check("name", "The name is required").not().isEmpty()],
  CollectionController.createCollection
);
router.get(
  "/",
  AuthMiddleware.validateAuthToken,
  CollectionController.getCollections
);
router.get(
  "/:id",
  AuthMiddleware.validateAuthToken,
  CollectionController.getCollection
);
router.put(
  "/:id",
  AuthMiddleware.validateAuthToken,
  [check("name", "The name is required").not().isEmpty()],
  CollectionController.updateCollection
);
router.delete(
  "/:id",
  AuthMiddleware.validateAuthToken,
  CollectionController.deleteCollection
);

export default router;
