import axios, { AxiosError } from "axios";
import React, { MouseEvent, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../app/store";
import { addTodo } from "../features/todoSlice";
import { Constants } from "../shared/constants";
import TodoCard from "./TodoCard";
import { ITodo } from "../shared/interfaces";
import { logOut, setError } from "../features/authSlice";
import { useNavigate } from "react-router-dom";

const Todos = (props: any) => {
  const selectedCollection = useSelector(
    (state: RootState) => state.collections.selectedCollection
  );
  const todos = useSelector((state: RootState) => state.todos.currentTodos);
  const [todoNameInput, setTodoNameInput] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();
  // Create Todo
  const handleCreateTodo = (
    event: MouseEvent<HTMLButtonElement>,
    collectionId: string
  ) => {
    event.preventDefault();
    if (!todoNameInput) return;
    // dispatch the action that adds the new todo to the redux store.
    const data = {
      name: todoNameInput,
      collectionId: collectionId
    };
    axios
      .post(Constants.APIBaseURL_dev + "todos/", data, {
        headers: {
          "x-auth-token": localStorage.getItem("token") || ""
        }
      })
      .then((response) => {
        const newTodo: ITodo = {
          _id: response.data._id,
          name: response.data.name,
          collectionId: response.data.collectionId,
          completed: response.data.completed,
          tags: response.data.tags
        };
        dispatch(addTodo(newTodo));
      })
      .catch((error: AxiosError) => {
        console.log("error :>> ", error);
        if (error.response) {
          dispatch(setError(error.response.data));
        }
        if (error.response?.status === 401) {
          dispatch(logOut(true));
          navigate("/");
        }
      })
      .finally(() => {
        // reset the input field (maybe dispatch a success message, the rror message sould be in the layout)
        setTodoNameInput("");
      });
  };

  return (
    <div className=" ">
      <p className="text-new-green uppercase text-base py-3 tracking-wider">
        {selectedCollection ? (
          <>Todos of {selectedCollection?.name}</>
        ) : (
          <> Select a collection</>
        )}
      </p>
      {selectedCollection &&
        todos.length !== 0 &&
        todos.map((todo, index) => {
          return (
            <TodoCard
              key={todo._id}
              index={index}
              _id={todo._id}
              name={todo.name}
              completed={todo.completed}
              tags={todo.tags}
              collectionId={todo.collectionId}
            />
          );
        })}
      {selectedCollection && (
        <div>
          <input
            className="mb-2 block w-full max-w-sm py-2 px-3 text-base text-new-form-grey border border-new-green-light border-opacity-50 bg-new-green-light bg-opacity-50 shadow-sm focus:bg-white focus:outline-none focus:ring-0 focus:border-new-green tracking-wider sm:text-base lg:text-xs"
            value={todoNameInput}
            onChange={(event) => {
              event.preventDefault();
              setTodoNameInput(event.target.value);
            }}
          />
          <button
            type="button"
            className="rounded-full py-2 px-12 text-white text-base focus:outline-none uppercase bg-new-green tracking-wider sm:text-base lg:text-xs"
            onClick={(event) =>
              handleCreateTodo(event, selectedCollection?._id)
            }
          >
            Create Todo
          </button>
        </div>
      )}
    </div>
  );
};
export default Todos;
