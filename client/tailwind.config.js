module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'new-green': '#2DAAA5',
        'new-green-light': '#95D4D2',
        'new-form-grey': '#9B9A9A',
        'new-form-grey-light': '#CECECE',
        'new-red': '#FF7979'
      }
    },
    fontFamily: {
      'opensans': ['"Open Sans"'],
      'helvetica': ['"Helvetica Neue"', '"Helvetica Neue Light"', 'Helvetica', 'Arial']
    },
   
  },
  plugins: [
    // require('@tailwindcss/forms'),
    // require('@tailwindcss/aspect-ratio')
  ],
}
