import { Result, ValidationError, validationResult } from "express-validator";

import { ICollection, CollectionModel } from "../models/Collection";

class CollectionController {
  // Create a collection
  async createCollection(req: any, res: any) {
    const errors: Result<ValidationError> = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const collection: ICollection = new CollectionModel(req.body);
      collection.owner = req.user.id;
      collection.save();
      res.json(collection);
    } catch (error) {
      console.log(error);
      res
        .status(500)
        .send("Unexpected error while creating the new collection");
    }
  }

  // Get all collections
  async getCollections(req: any, res: any) {
    const errors: Result<ValidationError> = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      let collections = await CollectionModel.find({ owner: req.user.id });
      res.json(collections);
    } catch (error) {
      console.log(error);
      res.status(500).send("Unexpected error while geting users collections");
    }
  }

  // Get collection
  async getCollection(req: any, res: any) {
    const errors: Result<ValidationError> = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      // check if the collection exists or not
      let collection = await CollectionModel.findById(req.params.id);
      if (!collection) {
        return res.status(400).json({ msg: "Collection does not exists" });
      }
      // check the user is the owner of the collection
      if (req.user.id !== collection.owner.toString()) {
        return res.status(401).json({ msg: "Not authorized" });
      }
      res.json(collection);
    } catch (error) {
      console.log(error);
      res.status(500).send("Unexpected error while geting the collection");
    }
  }

  // Update collection
  async updateCollection(req: any, res: any) {
    const errors: Result<ValidationError> = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { name } = req.body;
    let newCollection: any = {};
    if (name) {
      newCollection.name = name;
    }
    try {
      // check if the collection exists or not
      let collection = await CollectionModel.findById(req.params.id);
      if (!collection) {
        return res.status(400).json({ msg: "Collection does not exists" });
      }
      // check the user is the owner of the collection
      if (req.user.id !== collection.owner.toString()) {
        return res.status(401).json({ msg: "Not authorized" });
      }

      //update the collection
      collection = await CollectionModel.findOneAndUpdate(
        { _id: req.params.id },
        { $set: newCollection },
        { new: true }
      );
      res.json(collection);
    } catch (error) {
      console.log(error);
      res.status(500).send("Unexpected error while updating the collection");
    }
  }

  // Delete Collection
  async deleteCollection(req: any, res: any) {
    const errors: Result<ValidationError> = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      // check the collection is exits
      let collection = await CollectionModel.findById(req.params.id);
      if (!collection) {
        return res.status(400).json({ msg: "Collection does not exists" });
      }
      // check the user is the owner of the collection
      if (req.user.id !== collection.owner.toString()) {
        return res.status(401).json({ msg: "Not authorized" });
      }
      // delete the collection
      await CollectionModel.findByIdAndRemove(req.params.id);
      res.json({ msg: "Collection deleted" });
    } catch (error) {
      console.log(error);
      res.status(500).send("Unexpected error while deleting the collection");
    }
  }
}

export default new CollectionController();
