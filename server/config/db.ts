const mongoose = require('mongoose');
require('dotenv').config({ path: 'variables.env' });

export const connectDB = async () => {
    try {
        await mongoose.connect(process.env.MONGO_DB_CONNECTION_STRING);
        console.log('DB connected');
    } catch (error) {
        console.log(error);
        process.exit(1);
    }
};